﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CSSNotes.aspx.cs" Inherits="Assignment2a_RyanRobinson_B.CSSNotes" %>

<asp:Content ContentPlaceHolderID="conceptExplanation" runat="server">
    <h1>CSS Notes</h1>
    <h2>Tricky Concepts</h2>
    <h3>Floats</h3>
    <p>
       Floats are a way to position content on a page relative to other content instead of in an absolute position. They can often interact in
       unexpected ways with other content if you do not use a div with a container class and set the overflow of this in CSS.

       Another common problem is not clearing the float which can be seen in the first example. 
    </p>
</asp:Content>


<asp:Content ContentPlaceHolderID="codeByMe" runat="server">
    <h2>Example: Floats</h2>
    <%-- I'm not sure why this CodeBox tag is not working on any page. I thought it had to do with the web.config file but I can't see a problem there. --%>
    <asp:CodeBox ID="cssCodeOutput1" runat="server"
        SkinId="darkSkin" code="codeContainer" owner="me">
    </asp:CodeBox>
    <p>
        This code floats an element with the class of "logo" to the left and afterwards clears the float. The code also sets the size and top padding.
    </p>
</asp:Content>

<asp:Content ContentPlaceHolderID="codeByOthers" runat="server">
    <h2>Example: </h2>
    <asp:CodeBox ID="cssCodeOutput2" runat="server"
        SkinId="darkSkin" code="codeContainer" owner="other">
    </asp:CodeBox>
    <p>
        This code floats everything in the class of img-container to the left and sets the width that class takes up to 33.33% of the page.
    </p>
</asp:Content>

<asp:Content ContentPlaceHolderID="helpfulLinks" runat="server">
    <h2>Helpful Links</h2>
    <ul>
        <li><a href="https://www.w3schools.com/css/css_float.asp">W3Schools on Floats</a></li>
        <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/float">Mozilla on Floats</a></li>
        <li><a href="https://css-tricks.com/all-about-floats/">CSS-Tricks.com on Floats</a></li>
    </ul>
</asp:Content>