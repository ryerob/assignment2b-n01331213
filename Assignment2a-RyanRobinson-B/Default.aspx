﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2a_RyanRobinson_B._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Study Guide</h1>
        <p class="lead">A Mid Term study guide including javascript, Oracle SQL and CSS</p>
    </div>

    <div class="row">
        <div class="col-md-4" "col-sm-12">
            <h2>Javascript</h2>
            <p>
                This section includes information about loops and arrays in javascript with a couple of code examples and list of links.
            </p>
            <p>
                <a class="btn btn-default" href="JavascriptNotes.aspx">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4" "col-sm-12">
            <h2>CSS</h2>
            <p>
               Some information on floats.
            </p>
            <p>
                <a class="btn btn-default" href="CSSNotes.aspx">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4" "col-sm-12">
            <h2>Oracle SQL</h2>
            <p>
               Table joins in Oracle SQL with examples.
            </p>
            <p>
                <a class="btn btn-default" href="OracleSQLNotes">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
