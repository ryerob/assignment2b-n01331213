﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JavascriptNotes.aspx.cs" Inherits="Assignment2a_RyanRobinson_B.Javascript" %>

<asp:Content ContentPlaceHolderID="conceptExplanation" runat="server">
    <h1>JavaScript Notes</h1>
    <h2>Tricky Concepts</h2>
    <h3>For Loops</h3>
    <p>
        For Loops are used when you to want repeat an action for a known number of iterations. They have a variable that is usually
        represented by the letter "i". This letter gets either added to or subtracted from and checks a condition.
    </p>
    <h3>Do...While Loop</h3>
    <asp:CodeBox ID="jsCodeOutput1" runat="server"
        SkinId="light.skin" code="codeContainer" owner="me">
    </asp:CodeBox>
    <p>
        Do while loops are best used when you don't know the amount of times that the loop will run. This type of loop does an action 
        until a condition is met.
    </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="codeByMe" runat="server">
    <h2>Example: While Loop</h2>
  <asp:CodeBox ID="jsCodeOutput2" runat="server"
        SkinId="codebox" code="codeContainer" owner="me">
    </asp:CodeBox>
    <p>This was used in an assignment using an array to store a list of books and promt the user for input which it validates as a
       number between 1-10 and returns the corrsponding book on the list</p>
</asp:Content>
<asp:Content ContentPlaceHolderID="codeByOthers" runat="server">
    <h2>Example: For Loop</h2>
   
    <p>
        This code is from <a href="https://www.w3schools.com/js/js_objects.asp">W3Schools</a> and shows an example of an object named "person" with 4 
        properties and 4 property values. Functions can also be declared inside an object which makes then a method of that object.
    </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="helpfulLinks" runat="server">
    <h2>Helpful Links</h2>
    <ul>
        <li><a href="https://www.w3schools.com/jsref/jsref_dowhile.asp">W3Schools on While Loops</a></li>
        <li><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/do...while">MDN on While Loops</a></li>
    </ul>
</asp:Content>