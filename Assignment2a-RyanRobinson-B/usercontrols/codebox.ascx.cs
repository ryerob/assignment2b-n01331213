﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2a_RyanRobinson_B.usercontrols
{
    public partial class Codebox : System.Web.UI.UserControl

    {

        DataView CreateCodeSource()
        {
            DataTable codeContainer = new DataTable();


            DataColumn idx_col = new DataColumn();

            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codeContainer.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codeContainer.Columns.Add(code_col);

            List<string> cssCode = new List<string>(new string[]{
            "#logo {",
            "~width:70px;",
            "~height: 70px;",
            "~padding - top:50px;",
            "~float:left;",
            "~clear: left;",
            "}"
            });

            List<string> cssCode1 = new List<String>(new string[]{
              ".img-container {",
              "float: left;",
              "width: 33.33 %;",
              "padding: 5px;"
            });

            List<string> jsCode = new List<string>(new string[]{
            "books = [book1, book2, book3, book4, book5, book6, book7, book8, book9, book10];",
            "bookSelection = prompt('Which top 10 book would you like?', 'Pick a number: 1 - 10');",
            " ",
            "while (bookSelection === 'Pick a number: 1 - 10' || bookSelection < 1 || bookSelection > 10 || (isNaN(bookSelection))){",
            "~alert('Please enter a number between 1 and 10!');",
            "~bookSelection = prompt('Which top 10 book would you like?', 'Pick a number: 1 - 10');",
            "{",
            "alert('Number ' + bookSelection + ' on the list is \'' + books[bookSelection - 1] + '\''); "
            });

            List<string> jsCode1 = new List<string>(new string[]{
                "var person = {",
                "firstName:'John',",
                "lastName:'Doe',",
                "age:50,",
                "eyeColor:'blue'",
              "};"
            });

            List<string> sqlCode = new List<string>(new string[]{
            "UPDATE invoices",
            "SET terms_id = 2",
            "WHERE invoice_id IN", 
            "~(SELECT invoice_id",
            "~FROM vendors",
            "~WHERE default_terms_id = 2)"
            });

            List<string> sqlCode1 = new List<string>(new string[]{
            "SELECT invoice_due_date AS \"Due on\",", 
                "vendor_name AS \"Due from\",", 
                "payment_date AS \"Paid on\"", 
            "FROM invoices /* left side */",
            "RIGHT JOIN vendors /* right side */",
            "ON invoices.vendor_id = vendors.vendor_id",
            "ORDER BY \"Due on\" DESC",
            });

            int i = 0;
            foreach (string code_line in cssCode)
            {
                coderow = codeContainer.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codeContainer.Rows.Add(coderow);
            }
            DataView codeview = new DataView(codeContainer);
            return codeview;
        }
        // I know I need some logic to print out the proper piece of code to the datagrid depending on which page the user is on but I can't get the datagrids to print so its hard to test.
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
    }
}