﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SQLNotes.aspx.cs" Inherits="Assignment2a_RyanRobinson_B.OracleSQL" %>

<asp:Content ContentPlaceHolderID="conceptExplanation" runat="server">
    <h1>SQL Notes</h1>
    <h2>Tricky Concepts</h2>
    <h3>Joins</h3>
    <p>
        An inner-join is the default join used when using the "join" keyword. It only combines the rows that satisfy the join condition.
        <br />
        A self-join is where you join one table to a copy of itself.
        <br />
        A right join includes the rows from the first table that don't have a match in the second table.
        <br />
        A left join in clues rows from the second table that don't have matches in the first table.
        <br />
        An outer or full join includes rows from both tables that are not included in the other.
    </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="codeByMe" runat="server">
    <h2>SQL Example By Me</h2>
   
    <p>
       This sets all the terms ids in the invoices table to 2 for every invoice that has a default terms 
       id of 2. It took me a while to figure out that I needed "IN" instead of a "=".
    </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="codeByOthers" runat="server">
    <h2>SQL Example By</h2>
    
    <p>
        This is code by Simon that returns the invoice due date, vendor name and payment date from invoices and vendors. The tables are joined by matching the vendor id's
        in each table and since it is a right join all the instances of information that appears in the vendors that doesn't appear in invoices will be shown.
    </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="helpfulLinks" runat="server">
    <h2>Helpful Links</h2>
    <ul>
        <li><a href="https://docs.oracle.com/javadb/10.6.2.1/ref/rrefsqlj29840.html">Oracle's explanation of joins</a></li>
        <li><a href="https://www.w3schools.com/sql/sql_join.asp">W3schools SQL joins</a></li>
    </ul>
</asp:Content>

<%--I'm not sure why this page displays differently than the others since changing the content.--%>